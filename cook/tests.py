from django.utils import unittest
from cook.models import *

def create_test_group(members = 1):

    cs = ChoiceSet.objects.all()
    if not cs:
        choice_set = ChoiceSet()
        choice_set.text = 'cook'
        choice_set.save()
        for i in range(5):
            choice = Choice()
            choice.group = choice_set
            choice.choice_text = 'testing'
            choice.index = i+1
            choice.save()

    Member.create_member('0@test.com', '0', '0', None)
    m = Member.objects.get(user__first_name='0')
    Group.create_group('test', 'Europe/London', [], m)
    g = Group.objects.get(display_name='test')

    for i in range(members):
        Member.create_member('{}@test.com'.format(i), str(i), str(i), g.id)

    return g

def destroy_groups():
    groups = Group.objects.all()
    for group in groups:
        members = group.member_set.all()
        for i, member in enumerate(members):
            member.delete()
            member.user.delete()
        group.delete()

from cook.cron import decision_maker
class DecisionMaker(unittest.TestCase):
    
    def framework(self, choices, points, target_points, chef_name, n_diners, n_ni, target_codes, always_notify):

        # make sure group is destroyed first (always happens even if previous test failed)
        destroy_groups()

        # create new group
        self.group = create_test_group(members = len(choices))
        members = self.group.member_set.order_by('user__first_name')
        
        # set group as required for this test
        for i, member in enumerate(members):
            member.choice = choices[i]
            member.points = points[i]
            member.always_notify = always_notify[i]
            member.choice_active = False
            member.save()

        ### ACTIVATE_CHOICE TESTING
        self.group.next_t_decision = 12
        self.group.save
        decision_maker.activate_choice(self.group)
        self.group = Group.objects.get(display_name='test')

        # check member status
        members = self.group.member_set.order_by('user__first_name')
        for i, member in enumerate(members):
            self.assertEqual(member.choice_active, True)

        # check group status
        self.assertEqual(self.group.t_decision, 12)
        
        ### MAKE_DECISION TESTING
        chef, diners, not_involved = decision_maker.make_decision(self.group)
        
        # check points are correct
        members = self.group.member_set.order_by('user__first_name')
        for i, member in enumerate(members):
            self.assertEqual(member.points, target_points[i])
        
        # check arrays are correct
        if chef_name is not None:
            self.assertEqual(chef.get_name(), chef_name)
        else:
            self.assertEqual(len(chef), 0)
        self.assertEqual(len(diners), n_diners)
        self.assertEqual(len(not_involved), n_ni)

        ### GENERATE_EMAILS TESTING
        emails, codes = decision_maker.generate_emails(chef, diners, not_involved, [])

        # check correct emails are sent
        members = self.group.member_set.order_by('user__first_name')
        for i, member in enumerate(members):
            if not ('C' in target_codes[i] and not always_notify[i]):
                self.assertEqual(codes[member.get_name()], target_codes[i])

        # check everyone who should get an email has an email
        has_email = [not always for always in always_notify]
        for email in emails:
            has_email[int(email.to[0][0])] = True
        self.assertEqual(all(has_email), True)

        ### DEACTIVATE_CHOICE TESTING
        self.group.active = False
        self.group.save
        decision_maker.deactivate_choice(self.group)
        self.group = Group.objects.get(display_name='test')

        # check member status
        members = self.group.member_set.order_by('user__first_name')
        for i, member in enumerate(members):
            self.assertEqual(member.choice_active, False)
            self.assertEqual(member.choice, None)

        # check group status
        self.assertEqual(self.group.active, True)
            
    def test_111_000(self):
        self.framework(
            choices = [1,1,1], 
            points = [0,0,0], 
            target_points = [2,-1,-1], 
            chef_name = '0', 
            n_diners = 2, 
            n_ni = 0, 
            target_codes = ['A.2','B.1.2','B.1.2'],
            always_notify = [True,True,True]
            )

    def test_115_000(self):
        self.framework(
            choices = [1,1,5], 
            points = [0,0,0], 
            target_points = [1,-1,0], 
            chef_name = '0', 
            n_diners = 1, 
            n_ni = 1, 
            target_codes = ['A.2','B.1.1','C.1.2'],
            always_notify = [True,True,True]
            )

    def test_145_000(self):
        self.framework(
            choices = [1,4,5], 
            points = [0,0,0], 
            target_points = [1,-1,0], 
            chef_name = '0', 
            n_diners = 1, 
            n_ni = 1, 
            target_codes = ['A.2','B.1.1','C.1.2'],
            always_notify = [True,True,True]
            )

    def test_111_10n1(self):
        self.framework(
            choices = [1,1,1], 
            points = [1,0,-1], 
            target_points = [0,-1,1], 
            chef_name = '2', 
            n_diners = 2, 
            n_ni = 0, 
            target_codes = ['B.1.2','B.1.2','A.2'],
            always_notify = [True,True,True]
            )

    def test_255_000(self):
        self.framework(
            choices = [2,5,5], 
            points = [0,0,0], 
            target_points = [0,0,0], 
            chef_name = '0', 
            n_diners = 0, 
            n_ni = 2, 
            target_codes = ['A.1','C.1.1','C.1.1'],
            always_notify = [True,True,False]
            )

    def test_3542_000(self):
        self.framework(
            choices = [3,5,4,2], 
            points = [0,0,0,0], 
            target_points = [-1,0,-1,2], 
            chef_name = '3', 
            n_diners = 2, 
            n_ni = 1, 
            target_codes = ['B.1.2','C.1.3','B.1.2','A.2'],
            always_notify = [True,True,True,True]
            )

    def test_344_000(self):
        self.framework(
            choices = [3,4,4], 
            points = [0,0,0], 
            target_points = [2,-1,-1], 
            chef_name = '0', 
            n_diners = 2, 
            n_ni = 0, 
            target_codes = ['A.2.1','B.1.2.1','B.1.2.1'],
            always_notify = [True,True,True]
            )

    def test_444_000(self):
        self.framework(
            choices = [4,4,4], 
            points = [0,0,0], 
            target_points = [0,0,0], 
            chef_name = None, 
            n_diners = 3, 
            n_ni = 0, 
            target_codes = ['B.2.3','B.2.3','B.2.3'],
            always_notify = [True,True,True]
            )

    def test_321_000(self):
        self.framework(
            choices = [3,2,1], 
            points = [0,0,0], 
            target_points = [-1,-1,2], 
            chef_name = '2', 
            n_diners = 2, 
            n_ni = 0, 
            target_codes = ['B.1.2','B.1.2','A.2'],
            always_notify = [True,True,True]
            )

    def test_555_000(self):
        self.framework(
            choices = [5,5,5], 
            points = [0,0,0], 
            target_points = [0,0,0], 
            chef_name = None, 
            n_diners = 0, 
            n_ni = 3, 
            target_codes = ['C.2','C.2','C.2'],
            always_notify = [False,True,False]
            )

    def test_555555_521062(self):
        self.framework(
            choices = [5,5,5,5,5,5], 
            points = [5,2,1,0,-6,-2], 
            target_points = [5,2,1,0,-6,-2], 
            chef_name = None, 
            n_diners = 0, 
            n_ni = 6, 
            target_codes = ['C.2','C.2','C.2','C.2','C.2','C.2'],
            always_notify = [False,True,False,False,True,True]
            )

    def test_nnn_550(self):
        self.framework(
            choices = [None,None,None], 
            points = [5,-5,0], 
            target_points = [5,-5,0], 
            chef_name = None, 
            n_diners = 0, 
            n_ni = 3, 
            target_codes = ['C.2','C.2','C.2'],
            always_notify = [False,True,False]
            )

    def test_nnnnn_55220(self):
        self.framework(
            choices = [None,None,None,None,None], 
            points = [5,-5,2,-2,0], 
            target_points = [5,-5,2,-2,0], 
            chef_name = None, 
            n_diners = 0, 
            n_ni = 5, 
            target_codes = ['C.2','C.2','C.2','C.2','C.2'],
            always_notify = [False,True,False,False,False]
            )

    def test_4nn_527(self):
        self.framework(
            choices = [4,None,None], 
            points = [5,2,-7], 
            target_points = [5,2,-7], 
            chef_name = None, 
            n_diners = 1, 
            n_ni = 2, 
            target_codes = ['B.2.1','C.2','C.2'],
            always_notify = [False,True,False]
            )

    def test_455_000(self):
        self.framework(
            choices = [4,5,5], 
            points = [0,0,0], 
            target_points = [0,0,0], 
            chef_name = None, 
            n_diners = 1, 
            n_ni = 2, 
            target_codes = ['B.2.1','C.2','C.2'],
            always_notify = [True,True,True]
            )

    def test_445_000(self):
        self.framework(
            choices = [4,4,5], 
            points = [0,0,0], 
            target_points = [0,0,0], 
            chef_name = None, 
            n_diners = 2, 
            n_ni = 1, 
            target_codes = ['B.2.2','B.2.2','C.2'],
            always_notify = [True,True,True]
            )

    def test_335_10n1(self):
        self.framework(
            choices = [3,3,5], 
            points = [1,0,-1], 
            target_points = [0,1,-1], 
            chef_name = '1', 
            n_diners = 1, 
            n_ni = 1, 
            target_codes = ['B.1.1.1','A.2.1','C.1.2'],
            always_notify = [True,True,True]
            )
