#!/usr/bin/python

import os, sys, site, pytz, datetime

root = '/var/www/'
domain = 'www.house-bot.com'
# root = '/home/sp911/Code/'
# domain = '127.0.0.1:8000'

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir(root + '/house-bot/virtualenv/local/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append(root + '/house-bot')
sys.path.append(root + '/house-bot/house-bot')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "house_bot.settings")

# Activate the virtual env
activate_env = os.path.expanduser(root + 'house-bot/virtualenv/bin/activate_this.py')
execfile(activate_env, dict(__file__=activate_env))

from cook.models import Member, Group
from django.template.loader import render_to_string
from django.core import mail

def make_decision(group):
    # works out what each member is doing and updates the database
    # points and history
    
    # members who are not in
    not_involved = Member.objects.filter(cook_group=group).exclude(choice__lt=5)
    # members who are in with potential chef first
    involved = (Member.objects
                .filter(cook_group=group)
                .filter(choice__lt=5)
                .order_by('choice', 'points', 'user__first_name')
                )

    # initialise arrays assuming no chef
    chef = []
    diners = involved
    if diners:
        if diners[0].choice < 4:
            # get chef
            chef = diners[0]
            # remove chef from diners
            diners = diners.exclude(id=chef.id)

    # update history
    if chef:
        chef.last_choice = 2
        chef.save()
    for diner in diners:
        diner.last_choice = 1
        diner.save()
    for not_in in not_involved:
        not_in.last_choice = 0
        not_in.save()
        
    # update points
    if chef:
        chef.points += len(diners)
        chef.save()
        for diner in diners:
            diner.points -= 1
            diner.save()

    return chef, diners, not_involved

def generate_emails(chef, diners, not_involved, emails):
    # generates emails for each of the members    

    # log message code for debugging
    # A.X.X = chef 
    #   A.1 = no diners 
    #   A.2 = diners
    #     A.2.1 = rather not
    # B.X.X = diner
    #   B.1.X = have chef
    #     B.1.1 = 1 diner
    #     B.1.2 = >1 diners
    #       B.1.X.1 = chef = rather not
    #   B.2.X = no chef
    #     B.2.1 = 1 diner
    #     B.2.2 = 2 diners
    #     B.2.3 = >2 diners
    # C.X.X = not involved
    #   C.1.X = have chef
    #     C.1.1 = no diners
    #     C.1.2 = 1 diner
    #     C.1.3 = >1 diner
    #   C.2 = no chef
    codes = {}

    def create_general_info_email(subject, heading, paragraphs, send_to):
        content = { 'heading':heading, 'paragraphs':paragraphs }
        body = render_to_string('general_info.html', content)
        msg = mail.EmailMessage(subject, body, 'house-bot', send_to)
        msg.content_subtype = "html"
        
        return msg    

    # chef email        
    if chef:
        if diners:
            subject = "Congratulations chef!"
            heading = "Congratulations {}!".format(chef.get_name())
            paragraphs = [ 
                "You have been selected to cook tonight.",
                ("You are cooking for {} and yourself.</p>")
                .format(str.join(', ', [diner.get_name() for diner in diners]))
                ]
            codes[chef.get_name()] = 'A.2'
            if chef.choice == 3:
                paragraphs.append("The other group members have been let know that you did " + 
                                  "not really want to cook. Please let them know if dinner " + 
                                  "is going to be particularly late.")
                codes[chef.get_name()] = codes[chef.get_name()] + '.1'
        else:
            subject = "Dinner for one."
            heading = "Dinner for one."
            paragraphs = [
                ("Hey {}, you are just cooking for yourself tonight. Have a good evening.")
                .format(chef.get_name())
                ]
            codes[chef.get_name()] = 'A.1'
        emails.append(create_general_info_email(subject, heading, paragraphs, [chef.user.email]))

    # diner emails
    if chef:      
        subject = "You have a chef!"
        heading = "Your chef is {}!".format(chef.get_name())
        
        for diner in diners:
            other_diners = diners.exclude(id=diner.id)
            if other_diners:
                paragraphs = ["You will be dining with {} and your chef {}.".format(
                        str.join(', ', [other_diner.get_name() for other_diner in other_diners]),
                        chef.get_name())]
                codes[diner.get_name()] = 'B.1.2'
            else:
                paragraphs = ["You will be dining with your chef {}.".format(
                        chef.get_name())]
                codes[diner.get_name()] = 'B.1.1'
            if chef.choice == 3:
                paragraphs.append("The chef chose to cook as a last resort, so dinner " +
                                  "may be very quickly prepared and/or be later than usual.")
                codes[diner.get_name()] = codes[diner.get_name()] + '.1'
            paragraphs.append("Have a great evening.")
            emails.append(create_general_info_email(subject, heading, paragraphs, [diner.user.email]))
    else:
        subject = "No chef available."
        heading = "Sorry, no chef tonight."
        
        for diner in diners:
            other_diners = diners.exclude(id=diner.id)
            if other_diners:
                if len(other_diners) > 1:
                    names = [other_diner.get_name() for other_diner in other_diners]
                    diner_string = (
                        str.join(', ', [name for name in names[:-1]]) + 
                        ' and ' + names[-1])
                    codes[diner.get_name()] = 'B.2.3'
                else:
                    diner_string = other_diners[0].get_name()  
                    codes[diner.get_name()] = 'B.2.2'
                paragraphs = [
                    "Hello {}.".format(diner.get_name()),
                    "No one wanted to cook tonight. {} also wanted food. ".format(diner_string) +
                    "Can you organise something quick between yourselves.",
                    "See you tomorrow."] 
            else:
                paragraphs = [
                    "Hello {}.".format(diner.get_name()),
                    "You are the only one who wanted food tonight. " +
                    "Sorry that no one is available to cook for you.",
                    "See you tomorrow."]  
                codes[diner.get_name()] = 'B.2.1'
            emails.append(create_general_info_email(subject, heading, paragraphs, [diner.user.email]))

    # not involved emails
    subject = "Tonights result"
    if chef and not_involved:
        heading = "The chef is {}!".format(chef.get_name())

        for not_in in not_involved:
            if not_in.always_notify:
                if diners:
                    if len(diners) > 1:
                        names = [diner.get_name() for diner in diners]
                        diner_string = (
                            str.join(', ', [name for name in names[:-1]]) + 
                            ' and ' + names[-1])
                        codes[not_in.get_name()] = 'C.1.3'
                    else:
                        diner_string = diners[0].get_name()
                        codes[not_in.get_name()] = 'C.1.2'
                    p = "{} will be dining with {}.".format(diner_string, chef.get_name())
                else:
                    p = "{} is cooking for himself.".format(chef.get_name())
                    codes[not_in.get_name()] = 'C.1.1'

                paragraphs = [
                    "Hello {}.".format(not_in.get_name()),
                    p,
                    "Shame you couldn't make it tonight. " + 
                    "Let the chef know if you change your mind."]
                emails.append(create_general_info_email(subject, heading, 
                                                        paragraphs, [not_in.user.email]))

    else:
        heading = "No meal at home tonight."
        
        for not_in in not_involved:
            if not_in.always_notify:
                paragraphs = [
                    "Hello {}.".format(not_in.get_name()),
                    "There will be no meal prepared tonight.",
                    "See you tomorrow."]
                emails.append(create_general_info_email(subject, heading, 
                                                        paragraphs, [not_in.user.email]))
                codes[not_in.get_name()] = 'C.2'

    return emails, codes

def activate_choice(group):
    # enable group decision and update decision time at 1am
    group.t_decision = group.next_t_decision
    group.save()
    (
        Member.objects
        .filter(cook_group=group)
        .update(choice_active=True)
        )

def deactivate_choice(group):
    # disable group decision and activate new groups
    Member.objects.filter(cook_group=group).update(choice_active=False, choice=None)
    
    # activate new groups
    group.active = True
    group.save()    

now = datetime.datetime.utcnow()
groups = Group.objects.all()

# list of emails - sent at the end
emails = []

for group in groups:

    # get group time
    tz = pytz.timezone(group.t_zone)
    now_tz = now + tz.utcoffset(now)

    if now_tz.hour == 1:
        # activate choices
        activate_choice(group)
       
    # make decision at decision time
    if now_tz.hour == group.t_decision:
        # the brains
        chef, diners, not_involved = make_decision(group)
    
        # create emails - codes are returned but only needed for testing
        emails, codes = generate_emails(chef, diners, not_involved, emails)
    
        # deactivate choices
        deactivate_choice(group)    

# send emails
connection = mail.get_connection()
connection.open()
connection.send_messages(emails)
connection.close()
