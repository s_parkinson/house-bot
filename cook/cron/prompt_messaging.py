#!/usr/bin/python

import os, sys, site, pytz, datetime

root = '/var/www/'
domain = 'www.house-bot.com'
# root = '/home/sp911/Code/'
# domain = '127.0.0.1:8000'

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir(root + '/house-bot/virtualenv/local/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append(root + '/house-bot')
sys.path.append(root + '/house-bot/house-bot')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "house_bot.settings")

# Activate the virtual env
activate_env=os.path.expanduser(root + 'house-bot/virtualenv/bin/activate_this.py')
execfile(activate_env, dict(__file__=activate_env))

from cook.models import Member, Group
from django.core import mail
from django.template.loader import render_to_string

# get current time and members
now = datetime.datetime.utcnow()
members = Member.objects.filter(user__is_active=True)
emails = []

# loop through members and check if an email is required
for member in members:
    if member.cook_group is not None:
        body = None

        tz = pytz.timezone(member.cook_group.t_zone)
        now_tz = now + tz.utcoffset(now)
        
        if now_tz.hour == member.t_morning:
            subject = "Good morning from house-bot" 
            content = { 
                'domain':domain,
                'uid':member.user.username,
                'token':member.make_token(),
                }
            body = render_to_string('choice_email.html', content)
        elif now_tz.hour == member.cook_group.t_decision - 1 and member.choice is None:
            subject = "Reminder from house-bot"
            content = { 
                'domain':domain,
                'description':"You haven't made your choice and the deadline is approaching!",
                'uid':member.user.username,
                'token':member.make_token(),
                }
            body = render_to_string('choice_email.html', content)

        if body is not None:
            msg = mail.EmailMessage(subject, body, 'house-bot', [member.user.email])
            msg.content_subtype = "html"
            emails.append(msg)

# send emails
connection = mail.get_connection()
connection.open()
connection.send_messages(emails)
connection.close()
