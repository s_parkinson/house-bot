#!/usr/bin/python

import os, sys, site, pytz, datetime, json, urllib

root = '/var/www/'
domain = 'www.house-bot.com'
# root = '/home/sp911/Code/'
# domain = '127.0.0.1:8000'

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir(root + '/house-bot/virtualenv/local/lib/python2.7/site-packages')

sys.path.append(root + '/house-bot')
sys.path.append(root + '/house-bot/house-bot')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "house_bot.settings")

# Activate the virtual env
activate_env=os.path.expanduser(root + 'house-bot/virtualenv/bin/activate_this.py')
execfile(activate_env, dict(__file__=activate_env))

import oauth2 as oauth
import house_bot.secrets 

def oauth_req(url, http_method="GET", post_body=None,
        http_headers=None):

    APP_NAME = 'house-bot'
    CONSUMER_KEY = house_bot.secrets.CONSUMER_KEY
    CONSUMER_SECRET = house_bot.secrets.CONSUMER_SECRET
    TOKEN = house_bot.TOKEN
    TOKEN_SECRET = house_bot.TOKEN_SECRET
    consumer = oauth.Consumer(key=CONSUMER_KEY, secret=CONSUMER_SECRET)
    token = oauth.Token(key=TOKEN, secret=TOKEN_SECRET)
    client = oauth.Client(consumer, token)  

    resp, content = client.request(
        url,
        method=http_method,
        body=post_body,
        headers=http_headers,
        force_auth_header=True
    )

    return content

if __name__=="__main__":

    # house-bot twitter
    url = 'https://api.twitter.com/1.1/favorites/list.json?count=3&screen_name=_house_bot'
    responses = json.loads(oauth_req(url))

    f_twitter = open(root + 'house-bot/cook/social_data/twitter_favourites.json', 'w')
    json.dump(responses, f_twitter)
    f_twitter.close()

    # brompton twitter
    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    url = 'https://api.twitter.com/1.1/search/tweets.json?q=%%40BromptonBicycle&count=100&until=%s'%yesterday.strftime('%Y-%m-%d')
    responses = json.loads(oauth_req(url))

    twitPics = json.load(open(root + 'house-bot/cook/social_data/brompton_twitpics.json'))
    pic_urls = [r['entities']['media'][0]['media_url'] for r in twitPics]
    seen = set(pic_urls)
    
    tweets = responses['statuses']
    tweets.reverse()
    for tweet in tweets:
      if tweet['entities'].has_key('media'):
        if tweet['entities']['media'][0]['media_url'] not in seen:
          seen.add(tweet['entities']['media'][0]['media_url'])
          twitPics.append(tweet)
          if len(twitPics) > 1000:
            oldTweet = twitPics.pop(0)
            os.remove(oldTweet['entities']['media'][0]['thumb_url'])

    for tweet in twitPics:
      url = tweet['entities']['media'][0]['media_url']
      full_path = root + 'house-bot/cook/social_data/thumbs/'+url.split('/')[-1]
      mini_path = root + 'house-bot/cook/social_data/thumbs/mini_'+url.split('/')[-1][:-4]+'.jpg'
      if not os.path.isfile('thumbs/mini_'+url.split('/')[-1]):
        urllib.urlretrieve(url, full_path)
        os.system('convert -resize 200x200^ %s %s'%(full_path, mini_path))
        os.remove(full_path)
        tweet['entities']['media'][0]['thumb_url'] = 'http://www.house-bot.com/static/thumbs/'+mini_path.split('/')[-1]
    
    json.dump(twitPics, open(root + 'house-bot/cook/social_data/brompton_twitpics.json','w'))
    os.system('/var/www/house-bot/replace_static')

    # json.dump(twitPics, open('brompton_twitpics.json', 'w'))

    # for response in responses:
    #     print response['user']['description']

    # url = 'https://api.twitter.com/1.1/favorites/list.json?count=2&screen_name=HootanannyBrix'
    # responses = json.loads(oauth_req(url))
    # for response in responses:
    #     print response['user']['description']
