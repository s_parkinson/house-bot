#!/usr/bin/python

import os, sys, site, pytz, datetime

root = '/var/www/'
domain = 'www.house-bot.com'
# root = '/home/sp911/Code/'
# domain = '127.0.0.1:8000'

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir(root + '/house-bot/virtualenv/local/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append(root + '/house-bot')
sys.path.append(root + '/house-bot/house-bot')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "house_bot.settings")

# Activate the virtual env
activate_env=os.path.expanduser(root + 'house-bot/virtualenv/bin/activate_this.py')
execfile(activate_env, dict(__file__=activate_env))

from cook.models import Member, Group
from django.core import mail
from django.template.loader import render_to_string

# get current time and members
now = datetime.datetime.utcnow()
members = Member.objects.filter(user__is_active=True)
emails = []

# loop through members and check if an email is required
for member in members:
    if member.cook_group is not None:
        body = None

        tz = pytz.timezone(member.cook_group.t_zone)
        now_tz = now + tz.utcoffset(now)
        
        c = member.choice
        if c == None: c = -1
        open('/var/log/house.log','a').write('%s, %s, %i\n'%(now.__str__(), member.get_name(), c))
