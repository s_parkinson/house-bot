from django.db import models
from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login
from django.core.exceptions import ObjectDoesNotExist

import string, pytz, random

from django.core import mail
from django.template.loader import get_template
from django.template import Context
from django.conf import settings

from django.contrib.sites.models import get_current_site

# token generation
from datetime import datetime
from django.conf import settings
from django.utils.http import int_to_base36, base36_to_int
from django.utils.crypto import constant_time_compare, salted_hmac
from django.utils import six

class ChoiceSet(models.Model):
    text = models.CharField(max_length=200)

    def __unicode__(self):
        return unicode(self.text)

class Choice(models.Model):
    group = models.ForeignKey(ChoiceSet)
    choice_text = models.CharField(max_length=200)
    index = models.IntegerField()

    def __unicode__(self):
        return unicode(self.choice_text)

class Group(models.Model):
    id = models.CharField(primary_key=True, max_length=30)
    display_name = models.CharField(max_length=100)
    choice_set = models.ForeignKey(ChoiceSet)
    t_decision = models.IntegerField(default = 16)
    next_t_decision = models.IntegerField(default = 16)
    t_zone = models.CharField(max_length=30)
    active = models.BooleanField(default=False)

    @classmethod
    def create(cls):
        while True:
            id = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(30))
            try:
                group = Group.objects.get(id=id)
            except ObjectDoesNotExist:
                group = cls(id=id)
                return group

    def __unicode__(self):
        return unicode(self.display_name)

    @staticmethod
    def create_group(display_name, tz, emails, creator):
        group = Group.create()
        group.display_name = display_name
        group.t_zone = tz
        choices = ChoiceSet.objects.get(text='cook')
        group.choice_set = choices
        group.save()

        creator.cook_group = group
        now = datetime.utcnow()
        tz = pytz.timezone(group.t_zone)
        now_tz = now + tz.utcoffset(now)
        if now_tz.hour < group.t_decision:
            creator.choice_active = True
        creator.save()

        group.invite(emails, creator)

        return (0, 'Your group has been created successfully!')

    def invite(self, emails, invitor):
        
        connection = mail.get_connection()
        connection.open()
        messages = []
        html = get_template('registration/invitation.html')
        for email in emails:
            if email != '':
                content = Context({ 'email':email, 'cook_group':invitor.cook_group, 
                                    'member':invitor.get_name(),
                                    'domain':'www.house-bot.com'})
                subject, body = 'Your invitation to house-bot', html.render(content)
                from_email = 'house-bot'
                to = [email]
                messages.append(mail.EmailMessage(subject, body, from_email, to))
                messages[-1].content_subtype = "html"
        connection.send_messages(messages)
        connection.close()

    # def message(self, subject, body):
    #     from_email = 'house-bot'
    #     to = [member.user.email for member in Member.objects.filter(cook_group=self)]
    #     message = mail.EmailMessage(subject, body, from_email, to)
    #     message.content_subtype = "html"
    #     message.send()

class Member(models.Model):
    user = models.ForeignKey(User)
    cook_group = models.ForeignKey(Group, null=True, default=None)
    choice_active = models.BooleanField(default=False)
    choice = models.IntegerField(null=True, default=None)
    last_choice = models.IntegerField(null=True, default=None)
    points = models.FloatField(default = 0)
    t_morning = models.IntegerField(null=True, default=10)
    b_reminder = models.BooleanField(default=True)
    choice_token = models.CharField(null=True, default=None, max_length=200)
    always_notify = models.BooleanField(default=True)
    email_settings = models.BooleanField(default=True)
    email_history = models.BooleanField(default=True)

    def __unicode__(self):
        return unicode(self.user.username)

    def get_name(self):
        return self.user.first_name

    @staticmethod
    def create_member(email, password, display_name, cook_group):
        
        try:
            user = User.objects.get(email=email)
            return (1, 'A user already exists with this email address.')
        except ObjectDoesNotExist:
            while True:
                username = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(30))
                try:
                    user = User.objects.get(username=username)
                except ObjectDoesNotExist:
                    break

        user = User.objects.create_user(username, email, password)
        user.first_name = display_name
        user.save()
        
        member = Member()
        member.user = user
        if cook_group is not None:
            group = Group.objects.get(id=cook_group)
            member.cook_group = group
            token = None
            
            # decide whether choice is active or not
            now = datetime.utcnow()
            tz = pytz.timezone(group.t_zone)
            now_tz = now + tz.utcoffset(now)
            if now_tz.hour < group.t_decision:
                member.choice_active = True
        else:
            user.is_active = False
            user.save()
            
        member.last_choice = 0
        member.save()

        member.send_welcome_email()

        return (0, 'Your user account has been set up successfully. Welcome to house-bot')

    def send_welcome_email(self):
        html = get_template('registration/welcome_email.html')
        if not self.user.is_active:
            token = self.make_token()
        else:
            token = None
        content = Context({ 'member':self.get_name(), 'token':token, 'uid':self.user.username })
        subject, body = 'Welcome to house-bot', html.render(content)
        from_email = 'house-bot'
        to = [self.user.email]
        msg = mail.EmailMessage(subject, body, from_email, to)
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def login(email, password, request):
        
        try:
            user = User.objects.get(email=email)
        except:
            return (1, 'The email/password combination was incorrect.')
        user = authenticate(username=user.username, password=password)
        if user is not None:
            login(request, user)
            return (0, 'You are now logged in')
        else:
            return (1, 'The email/password combination was incorrect.')

    """
    Strategy object used to generate and check tokens for member.
    """
    def make_token(self):
        """
        Returns a token that can be used once to do a password reset
        for the given user.
        """
        token = self._make_token_with_timestamp(self._now())
        self.choice_token = token
        self.save()
        return token

    def check_token(self, token):
        """
        Check that a password reset token is correct for a given user.
        """
        if self.choice_token == token:
            # set token to something unguessable
            self.choice_token = self._make_token_with_timestamp(self._now())
            return True
        
        return False

    def _make_token_with_timestamp(self, timestamp):
        user = self.user

        # timestamp is number of days since 2001-1-1.  Converted to
        # base 36, this gives us a 3 digit string until about 2121
        ts_b36 = int_to_base36(timestamp)

        # By hashing on the internal state of the user and using state
        # that is sure to change (the password salt will change as soon as
        # the password is set, at least for current Django auth, and
        # last_login will also change), we produce a hash that will be
        # invalid as soon as it is used.
        # We limit the hash to 20 chars to keep URL short
        key_salt = "django.contrib.auth.tokens.PasswordResetTokenGenerator"

        # Ensure results are consistent across DB backends
        login_timestamp = user.last_login.replace(microsecond=0, tzinfo=None)

        value = (six.text_type(user.pk) + user.password +
                six.text_type(login_timestamp) + six.text_type(timestamp))
        hash = salted_hmac(key_salt, value).hexdigest()[::2]

        return "%s-%s" % (ts_b36, hash)

    def _now(self):
        now = datetime.utcnow()
        return now.hour*now.minute*now.second*now.microsecond

