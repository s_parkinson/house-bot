from django import forms
from captcha.fields import CaptchaField
from django.core.validators import validate_email
from django.forms import CharField, Textarea, ValidationError
from django.utils.translation import ugettext as _
import re
from django.utils import timezone
from cook.models import Member
from django.contrib.auth import authenticate
from django.contrib.auth.forms import PasswordResetForm as auth_PasswordResetForm        

class EditHistoryForm(forms.Form):

    def __init__(self, *args, **kwargs):
        
        group = kwargs.pop('group')

        super(EditHistoryForm, self).__init__(*args, **kwargs)

        for member in group.member_set.all().order_by('-last_choice', 'user__first_name'):

            key = 'm_id:' + str(member.id)
            try:
                initial = args[0][key]
            except:
                initial = member.last_choice

            self.fields[key] = forms.ChoiceField(
                label=member.user.first_name,
                choices=( [2, 'chef'], [1, 'diner'], [0, '-'] ),
                initial=initial,
                required=False
                )

class PasswordUpdateForm(forms.Form):
    
    old_password = forms.CharField(widget=forms.PasswordInput, label="Enter your old password")
    password1 = forms.CharField(widget=forms.PasswordInput, label="Choose a new password")
    password2 = forms.CharField(widget=forms.PasswordInput, label="Repeat the password")
    
    def __init__(self, *args, **kwargs):

        self.user = kwargs.pop('user')
            
        super(PasswordUpdateForm, self).__init__(*args, **kwargs)

        for field in self.fields.keys():
            self.fields[field].widget.attrs.update({'class' : 'form_input'})

    def clean(self):
        cleaned_data = super(PasswordUpdateForm, self).clean()

        old_password = cleaned_data.get("old_password")
        user = authenticate(username=self.user.username, password=old_password)

        if user is not None:
            password1 = cleaned_data.get("password1")
            password2 = cleaned_data.get("password2")
            if password1 != password2:
                raise forms.ValidationError("New passwords must be identical.")
            return cleaned_data
        else:
            raise forms.ValidationError("You did not enter your old password correctly.")

class AccountSettingsForm(forms.Form):
    
    email = forms.EmailField(label="Your email address")
    display_name = forms.CharField(max_length=100, label="Your name")
    
    t_morning = forms.TypedChoiceField(
        choices = ((0,0)),
        coerce = int,
        empty_value = None,
        label="Morning mail hour",
        required=False,
        )
    b_morning = forms.BooleanField(
        label="Morning mail",
        required=False,
        )
    b_reminder = forms.BooleanField(
        label="Reminder mail",
        required=False,
        )
    b_always_notify = forms.TypedChoiceField(
        choices = zip([1,0], ['always','only when I am in']),
        coerce = int,
        label="Result mail",
        )
    b_settings = forms.BooleanField(
        label="Group update mail",
        required=False,
        )
    b_history = forms.BooleanField(
        label="History edit mail",
        required=False,
        )
    
    def __init__(self, *args, **kwargs):

        member = kwargs.pop('member')

        super(AccountSettingsForm, self).__init__(*args, **kwargs)

        r = range(1, member.cook_group.t_decision - 1)
        hr_time = ['{}:00'.format(x) for x in r]
        self.fields['t_morning'].choices = zip(r, hr_time)
            
        if member is not None:
            self.fields['email'].initial = member.user.email
            self.fields['display_name'].initial = member.user.first_name

            if member.t_morning is not None:
                self.fields['t_morning'].initial = member.t_morning
                self.fields['b_morning'].initial = True
            else:
                self.fields['b_morning'].initial = False

            if member.b_reminder:
                self.fields['b_reminder'].initial = True
            else:
                self.fields['b_reminder'].initial = False

            self.fields['b_always_notify'].initial = member.always_notify
            self.fields['b_settings'].initial = member.email_settings
            self.fields['b_history'].initial = member.email_history

        for field in self.fields.keys():
            self.fields[field].widget.attrs.update({'class' : 'form_input'})

    def clean(self):
        cleaned_data = super(AccountSettingsForm, self).clean()
        
        # check t_morning if not b_morning - if t_morning is invalid then cleaned data will not 
        # have a value for it
        if cleaned_data.has_key('b_morning') and cleaned_data.has_key('t_morning'):
            if cleaned_data['b_morning'] and cleaned_data['t_morning'] is None:
                raise forms.ValidationError("You must give a morning email time if you want to receive one")
        
        return cleaned_data

class EmailsListField(CharField):

    # widget = Textarea(attrs={'placeholder': 'seperate email addresses with comma, space, semi-colon or enter each on a new line.'})
    widget = Textarea(attrs={'placeholder': 'my_friends@googliemail.com, lets_eat@house-bot.com'})

    def clean(self, value):

        super(EmailsListField, self).clean(value)

        email_separator_re = re.compile(r'[^\w\.\-\+@_]+')

        if value.replace(' ','') == '':
            return []

        emails = email_separator_re.split(value)

        if not emails:
            return []

        for email in emails:  
            if email != '':
                validate_email(email)

        return emails

class GroupSettingsForm(forms.Form):
    
    display_name = forms.CharField(
        max_length=100, 
        label="Your group name", 
        help_text="100 characters max."
        )

    t_decision = forms.TypedChoiceField(
        choices = zip(range(11, 24), ['{}:00'.format(x) for x in range(11, 24)]),
        coerce = int,
        empty_value = None,
        label="Decision hour",
        )

    tz = forms.ChoiceField(
        label="Your timezone",
        choices = zip(timezone.pytz.common_timezones,timezone.pytz.common_timezones), 
        )

    email_addresses = EmailsListField(
        label="Enter friends email addresses to invite them to your group.",
        help_text="People can only join your group via a link from an invite email.",
        required=False
        )
    
    def __init__(self, *args, **kwargs):

        if kwargs.has_key('group'):
            group = kwargs.pop('group')
        else:
            group = None

        super(GroupSettingsForm, self).__init__(*args, **kwargs)

        if group is not None:
            self.fields['display_name'].initial = group.display_name
            self.fields['t_decision'].initial = group.next_t_decision
            self.fields['tz'].initial = group.t_zone

        for field in self.fields.keys():
            self.fields[field].widget.attrs.update({'class' : 'form_input'})

from django.contrib.auth.tokens import default_token_generator
from django.core import mail
from django.contrib.sites.models import get_current_site
from django.utils.http import int_to_base36
from django.template import loader
from django.template.loader import get_template
from django.template import Context

class PasswordResetForm(auth_PasswordResetForm):

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """

        for user in self.users_cache:
            site_name = 'house-bot.com'
            domain = 'www.house-bot.com'
            c = {
                'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': int_to_base36(user.pk),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': use_https and 'https' or 'http',
                }
            context = Context(c)
            subject = loader.render_to_string(subject_template_name, c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            html = get_template(email_template_name)
            body = html.render(context)
            from_email = 'house-bot'
            to = [user.email]
            email = mail.EmailMessage(subject, body, from_email, to)
            email.content_subtype = "html"
            email.send()

class LoginForm(forms.Form):
    
    email = forms.EmailField(label="Your email address")
    password = forms.CharField(widget=forms.PasswordInput, label="Your password")
    remember = forms.BooleanField(
        label="Remember me",
        required=False,
        initial=False
        )
    
    def __init__(self, *args, **kwargs):

        super(LoginForm, self).__init__(*args, **kwargs)

        for field in self.fields.keys():
            self.fields[field].widget.attrs.update({'class' : 'form_input'})

class PasswordForm(forms.Form):
    
    password = forms.CharField(widget=forms.PasswordInput, label="Your password")

class SignUpForm(forms.Form):
    
    display_name = forms.CharField(max_length=100, 
                                   label="First name / nickname")
    email = forms.EmailField(label="Your email address")
    password1 = forms.CharField(widget=forms.PasswordInput, label="Choose a password")
    password2 = forms.CharField(widget=forms.PasswordInput, label="Repeat the password")
    captcha = CaptchaField()

    def __init__(self, *args, **kwargs):
        
        if kwargs.has_key('email'):
            email = kwargs.pop('email')
        else:
            email = None

        super(SignUpForm, self).__init__(*args, **kwargs)

        if email is not None:
            self.fields['email'].initial = email
            self.fields['email'].widget.attrs['readonly'] = True

        for field in self.fields.keys():
            self.fields[field].widget.attrs.update({'class' : 'form_input'})

    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")
        if password1 != password2:
            raise forms.ValidationError("Passwords must be identical.")
        
        return cleaned_data
    
class GroupCreationForm(forms.Form):
    
    display_name = forms.CharField(
        max_length=100, 
        label="Name your group", 
        help_text="100 characters max."
        )

    tz = forms.ChoiceField(
        label="Your timezone",
        choices = zip(timezone.pytz.common_timezones,timezone.pytz.common_timezones)
        )

    email_addresses = EmailsListField(
        label="Your fellow group members email addresses.", 
        help_text="People can only join your group via a link from an invite email. More members can be added later via the group settings menu.",
        required=False
        )

    def __init__(self, *args, **kwargs):
        
        if kwargs.has_key('tz'):
            tz = kwargs.pop('tz')
        else:
            tz = None

        super(GroupCreationForm, self).__init__(*args, **kwargs)

        if tz is not None:
            self.fields['tz'].initial = tz

        for field in self.fields.keys():
            self.fields[field].widget.attrs.update({'class' : 'form_input'})

from contact_form.forms import ContactForm
class MyContactForm(ContactForm):
    captcha = CaptchaField()
    
    def __init__(self, data=None, files=None, request=None, *args, **kwargs):
        
        super(MyContactForm, self).__init__(data=data, files=files, request=request, *args, **kwargs)
        
        if request.user.is_authenticated():
            self.fields['email'].initial = request.user.email
            self.fields['name'].initial = request.user.first_name
