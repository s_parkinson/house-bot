from django.contrib import admin
from cook.models import Member, Group, Choice, ChoiceSet

admin.site.register(Member)
admin.site.register(Group)
admin.site.register(Choice)
admin.site.register(ChoiceSet)
