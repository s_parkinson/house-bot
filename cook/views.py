from django.shortcuts import get_object_or_404, render, redirect
from django.template import loader, RequestContext, Context
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.core.urlresolvers import reverse

from django.views.generic.edit import FormView

from cook.models import Member, Group
from cook.forms import *
from django.conf import settings
import pygeoip
import json

from django.contrib.auth import logout
from django.forms.util import ErrorList
from django.core import mail

def index(request):

    if request.user.is_authenticated() and request.user.is_active:
        return logged_in_index(request)
    elif request.user.is_authenticated():
        return render(request, 'registration/verification_required.html')
    else:
        return logged_out_index(request)

@login_required
def logged_in_index(request):

    member = get_object_or_404(Member, user=request.user)
    if member.cook_group is None:
        return redirect('create group')
    
    if member.choice_active:
        # set edit text
        edit_text = 'edit last'

        # obtain input from web buttons
        if request.POST.has_key('index'):
            get_choice = (member.cook_group.choice_set.choice_set.get(index=request.POST['index'])).index
            member.choice = get_choice
            log_save(member, 0)
            member.save()
        elif request.POST.has_key('reset'):
            member.choice = None
            log_save(member, 1)
            member.save()
        
        # retake user from mysql to avoid incorrect equalities issues in template
        member = get_object_or_404(Member, user=request.user)
        
        context = Context({ 'member':member, 'url':reverse('cook.views.index') })
        if member.choice is not None:
            home_content = render_to_string('choice_made.html', context, context_instance=RequestContext(request))
        else:
            home_content = render_to_string('choice_form.html', context, context_instance=RequestContext(request))

    else:
        # set edit text
        edit_text = 'edit'

        sorted_members = member.cook_group.member_set.all().order_by('-last_choice', 'user__first_name')
        context = Context({ 'member':member, 'sorted_members':sorted_members })
        home_content = render_to_string('decision_made.html', context, context_instance=RequestContext(request))
                
    home = loader.get_template('home.html')
    return render(request, 'home.html', { 'home_content':home_content, 
                                          'member':member,
                                          'edit_text':edit_text})

@never_cache
def quick_choice(request, uid=None, token=None, choice=None):
    
    warning = None
    if uid is None:
        messages.add_message(request, messages.ERROR, 
                             "The quick choice url requires a user id.")
        return redirect('/')

    # get member
    user = get_object_or_404(User, username=uid)
    member = get_object_or_404(Member, user=user)

    # check user has a group
    if member.cook_group is None:
        messages.add_message(request, messages.ERROR, 
                             "You do not have a group. Please log in and create one or obtain an invitation from an existing group.")
        return redirect('/')

    # check user matches logged in user
    if request.user.is_authenticated():
        if request.user != user:
            messages.add_message(request, messages.ERROR, 
                                 'You are currently logged in as {}. Logout to use this link and make a choice for {}'
                                 .format(request.user.first_name, user.first_name))
            return redirect('/')
    
    if member.choice_active:
        # set edit text
        edit_text = 'edit last'

        # update choice from email link if given
        if choice is not None and token is not None:
            if member.check_token(token):
                member.choice = choice
                log_save(member, 2)
                member.save()
            else:
                messages.add_message(request, messages.ERROR, 
                                     "The link from this email has expired or has already been used. Your choice was not registered. Feel free to make changes using the options below.")
  
        # if logged in redirect to home
        if request.user.is_authenticated():
            return redirect('/')

        # obtain input from web buttons
        if request.POST.has_key('index'):
            get_choice = (member.cook_group.choice_set.choice_set.get(index=request.POST['index'])).index
            member.choice = get_choice
            log_save(member, 3)
            member.save()
        elif request.POST.has_key('reset'):
            member.choice = None
            log_save(member, 4)
            member.save()
        
        # retake user from mysql to avoid incorrect equalities issues in template
        member = get_object_or_404(Member, user=user)
        
        context = Context({ 'member':member, 'url':reverse('quick choice', args=(uid, )) })
        if member.choice is not None:
            home_content = render_to_string('choice_made.html', context, context_instance=RequestContext(request))
        else:
            home_content = render_to_string('choice_form.html', context, context_instance=RequestContext(request))

    else:
        # set edit text
        edit_text = 'edit'

        context = Context({ 'member':member })
        home_content = render_to_string('decision_made.html', context, context_instance=RequestContext(request))

    return render(request, 'quick_choice.html', { 'home_content':home_content, 'warning':warning, 'member':member })

def logged_out_index(request):

    f_twitter = open(settings.TWITTER_FAVOURITES, 'r')
    responses = json.load(f_twitter)
    context = { 'message_0':responses[0]['text'],
                'message_0_u':responses[0]['user']['screen_name'],
                'message_1':responses[1]['text'],
                'message_1_u':responses[1]['user']['screen_name'],
                'message_2':responses[2]['text'],
                'message_2_u':responses[2]['user']['screen_name'], }

    return render(request, 'welcome.html', context)

def login_view(request):

    if request.user.is_authenticated():
        messages.add_message(request, messages.WARNING, 
                             "You are already logged in.")
        return redirect('/')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            if not form.cleaned_data['remember']:
                request.session.set_expiry(0)

            code, detail = Member.login(email, password, request)
            if code == 0:
                messages.add_message(request, messages.INFO, 
                                     'You are now logged in')
                member = Member.objects.get(user=request.user)
                return redirect('/')
            else:
                messages.add_message(request, messages.ERROR, 
                                     detail)
    else:
        form = LoginForm()

    return render(request, 'login.html', { 'form':form })

@login_required
def logout_view(request):
    
    logout(request)
    messages.add_message(request, messages.INFO, 
                         'You have been logged out')
    return redirect('/')

def sign_up_to_group(request):

    if request.method == 'POST':
        form = PasswordForm(request.POST)
        group = get_object_or_404(Group, id=request.session['cook_group'])
        
        if form.is_valid():
            user = User.objects.get(email=request.session['email'])
            user = authenticate(username=user.username, password=form.cleaned_data['password'])
            if user is None:
                errors = form._errors.setdefault("password", ErrorList())
                errors.append(u"Incorrect password.")
            else:
                member = get_object_or_404(Member, user=user)
                old_group = None
                if member.cook_group:
                    g_members = Member.objects.filter(cook_group = member.cook_group)
                    if len(g_members) == 1:
                        old_group = member.cook_group
                    else:
                        point_share = member.points/(len(g_members) - 1)
                        for g_member in g_members:
                            g_member.points += point_share
                            log_save(g_member, 5)
                            g_member.save()
                
                member.cook_group = group
                if old_group is not None:
                    group.delete()
                    
                log_save(member, 6)
                member.save()

                messages.add_message(request, messages.INFO, 
                                     'Your cook group has been updated')
                return redirect('/')

    else:
        request.session['email'] = request.GET.get('e')
        request.session['cook_group'] = request.GET.get('g')
        group = get_object_or_404(Group, id=request.session['cook_group'])
        
        users = User.objects.filter(
            email=request.session['email']
            )
        if users:
            if request.user.is_authenticated():
                if users[0] != request.user:
                    messages.add_message(request, messages.ERROR, 
                                         'You are logged in as a different user. Please logout to perform this operation.')
                    return redirect('/')
            form = PasswordForm()
        else:
            return sign_up(request)

    return render(request, 'registration/sign_up_to_group.html', 
                  { 'cook_group':group, 'form':form })

def close_account(request):

    if request.method == 'POST':
        form = PasswordForm(request.POST)
        
        if form.is_valid():
            user = request.user
            logout(request)
            
            member = get_object_or_404(Member, user=user)
            if member.cook_group:
                g_members = Member.objects.filter(cook_group = member.cook_group)
                if len(g_members) == 1:
                    group = member.cook_group
                else:
                    point_share = member.points/(len(g_members) - 1)
                    for g_member in g_members:
                        g_member.points += point_share
                        log_save(g_member, 7)
                        g_member.save()
                    group = None

            member.delete()
            user.delete()
            if group is not None:
                group.delete()

            messages.add_message(request, messages.INFO, 
                                 'Your account has been closed. We are sorry to see you go.')
            return redirect('/')

    else:
        form = PasswordForm()

    return render(request, 'registration/close_account.html', { 'form':form })

def sign_up(request):

    if request.user.is_authenticated():
        messages.add_message(request, messages.WARNING, 
                             "You are already logged in and so cannot sign up again.")
        return redirect('/')
        
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            display_name = form.cleaned_data['display_name']
            password = form.cleaned_data['password1']

            if request.session.has_key('cook_group'):
                email=request.session['email']
                cook_group=request.session['cook_group']
            else:
                email = form.cleaned_data['email']
                cook_group = None

            code, detail = Member.create_member(email, password, display_name, cook_group)
            
            if code == 0:
                messages.add_message(request, messages.INFO, 
                                     detail)

                code, detail = Member.login(email, password, request)
                if code == 0:
                    
                    return redirect('/')
                else:
                    messages.add_message(request, messages.ERROR, 
                                         detail)
            else:
                messages.add_message(request, messages.ERROR, 
                                     detail)

    else:
        if request.session.has_key('email') and request.session.has_key('cook_group'):
            form = SignUpForm(email=request.session['email'])
        else:
            form = SignUpForm()

    if request.session.has_key('cook_group'):
        group = get_object_or_404(Group, id=request.session['cook_group'])
    else:
        group = None

    return render(request, 'registration/sign_up.html', { 'form':form, 'group':group })

def verify(request, token=None, uid=None):
    assert(token is not None and uid is not None)
    
    user = get_object_or_404(User, username=uid)

    if request.user.is_authenticated() and user.id != request.user.id:
        messages.add_message(request, messages.ERROR, 
                             "You are logged in as a different user. Log out to activate your account.")
        return redirect('/')

    if user.is_active:
        if request.user.is_authenticated():
            messages.add_message(request, messages.ERROR, 
                                 "Your account is already activated. No need to do this")
        else:
            messages.add_message(request, messages.ERROR, 
                                 "Your account is already activated. No need to do this, just log in.") 
            return redirect('login')
    member = get_object_or_404(Member, user=user)
    if member.check_token(token):
        user = member.user
        user.is_active = True
        user.save()
        if request.user.is_authenticated():
            messages.add_message(request, messages.INFO, 
                                 'You account is activated. Enjoy using House-Bot.')
        else:
            messages.add_message(request, messages.INFO, 
                                 'You account is activated. Enjoy using House-Bot.')
            return redirect('login')
    else:
        messages.add_message(request, messages.ERROR, 
                             "There was a problem validating the account using this link. Log in to receive a new activation email or try again.") 

    return redirect('/')

@login_required
def resend_verification(request):
    member = get_object_or_404(Member, user=request.user)
    member.send_welcome_email()
    messages.add_message(request, messages.INFO, 
                             'We have sent you a new welcome email. Check your inbox and junk mail.')
    return redirect('/')
        
geoip_db_loaded = False
geoip_db = None

def load_db():
    global geoip_db
    geoip_db = pygeoip.GeoIP(settings.GEOIP_DATABASE, pygeoip.MEMORY_CACHE)

    global geoip_db_loaded
    geoip_db_loaded=True

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return ip

@login_required
def create_group(request):

    if not request.user.is_active:
        return redirect('/')

    member = get_object_or_404(Member, user=request.user)
    if member.cook_group is not None:
        messages.add_message(request, messages.WARNING, 
                             "You are already a member of a group.")
        return redirect('/')

    if request.method == 'POST':
        form = GroupCreationForm(request.POST)

        if form.is_valid():
            display_name = form.cleaned_data['display_name']
            tz = form.cleaned_data['tz']
            emails = form.cleaned_data['email_addresses']

            code, detail = Group.create_group(display_name, tz, emails, member)
            
            if code == 0:
                messages.add_message(request, messages.INFO, 
                                     detail)
                return redirect('/')
            else:
                messages.add_message(request, messages.ERROR, 
                                     detail)
    else:
        if not geoip_db_loaded:
            load_db()

        ip = get_client_ip(request)
        if ip == '127.0.0.1':
            ip = '74.125.224.72' # Google - centre of the world
        tz = geoip_db.time_zone_by_addr(ip)
        
        form = GroupCreationForm(tz=tz)
        
    return render(request, 'group_set_up.html', { 'form':form, 'display_name':request.user.first_name, 'no_settings':True })

@login_required
def password_update_view(request):

    if request.method == 'POST':
        form = PasswordUpdateForm(request.POST, user=request.user)
        if form.is_valid():
            password = form.cleaned_data['password1']

            user = get_object_or_404(User, id=request.user.id)
            user.set_password(password)
            user.save()
            
            messages.add_message(request, messages.INFO, 
                                 'Password updated successfully')
            
            return redirect('/')
    else:
        form = PasswordUpdateForm(user=request.user)

    return render(request, 'password_update.html', { 'form':form })

@login_required
def settings_account_view(request):

    member = get_object_or_404(Member, user=request.user)

    if request.method == 'POST':
        form = AccountSettingsForm(request.POST, member=member)
        if form.is_valid():
            valid = True

            email = form.cleaned_data['email']
            display_name = form.cleaned_data['display_name']

            # check email validity
            if request.user.email != email:
                users = User.objects.filter(
                    email=email
                    )
                if users:
                    errors = form._errors.setdefault("email", ErrorList())
                    errors.append(u"Another user exists with this email address.")
                    valid = False
                else:
                    heading = "Email address update."
                    paragraphs = [
                        "You have updated the email address associated with your House-Bot account.", 
                        "Your old email address was {} and your new one is {}.".format(request.user.email, email)
                        ]
                    content = { 'heading':heading, 'paragraphs':paragraphs }
                    body = render_to_string('general_info.html', content)
                    subject = 'Email address change at house-bot'
                    from_email = 'house-bot'
                    msg = mail.EmailMessage(subject, body, 'house-bot', [email, member.user.email])
                    msg.content_subtype = "html"
                    msg.send()
                    request.user.email = email

            request.user.first_name = display_name
            
            if not form.cleaned_data['b_morning']:
                member.t_morning = None
                # TODO: this doesn't clear the form field
                form.fields['t_morning'].initial = None
            else:
                member.t_morning = form.cleaned_data['t_morning']

            member.b_reminder = form.cleaned_data['b_reminder']

            member.email_settings = form.cleaned_data['b_settings']
            member.email_history = form.cleaned_data['b_history']
            member.always_notify = form.cleaned_data['b_always_notify']
        
            if valid:
                request.user.save()
                log_save(member, 8)
                member.save()
                messages.add_message(request, messages.INFO, 
                                 'Settings updated successfully')

    else:
        form = AccountSettingsForm(member=member)

    return render(request, 'settings_account.html', 
                  { 'form':form, 'url':reverse('settings account'), 'account':True })

@login_required
def settings_group_view(request):

    member = get_object_or_404(Member, user=request.user)
    group = member.cook_group

    if request.method == 'POST':
        form = GroupSettingsForm(request.POST)
        if form.is_valid():
            display_name = form.cleaned_data['display_name']
            t_decision = form.cleaned_data['t_decision']
            tz = form.cleaned_data['tz']

            if (display_name != group.display_name or 
                t_decision != group.next_t_decision or
                tz != group.t_zone):
                group.display_name = display_name
                group.next_t_decision = t_decision
                group.t_zone = tz
                group.save()

                t_change = group.next_t_decision != group.t_decision

                html = get_template('group_settings_update.html')
                content = Context({ 'cook_group':group, 'user':request.user, 't_change':t_change })
                subject, body = 'Your house-bot group settings have changed.', html.render(content)
                from_email = 'house-bot'
                to = [member.user.email for member in 
                      Member.objects.filter(cook_group=group).filter(email_settings=True)]
                message = mail.EmailMessage(subject, body, from_email, to)
                message.content_subtype = "html"
                message.send()

                msg = 'Settings updated successfully. Decision time changes will be made at 1am tomorrow.'
                messages.add_message(request, messages.INFO, 
                                     msg)

            emails = form.cleaned_data['email_addresses']
            if emails:
                group.invite(emails, member)

                messages.add_message(request, messages.INFO, 
                                     'Invite emails delivered')

    else:
        form = GroupSettingsForm(group=group)

    return render(request, 'settings_group.html', 
                  { 'form':form, 'url':reverse('settings group'), 'members':group.member_set.all() })

@login_required
def edit_history_view(request):

    edit_member = get_object_or_404(Member, user=request.user)
    group = edit_member.cook_group
    if request.method == 'POST':
        form = EditHistoryForm(request.POST, group=group)
        if form.is_valid():
            # add up points available before and now
            b_points = 0
            a_points = 0
            # add up number of cookers 
            b_cookers = 0
            a_cookers = 0
            # strings for email 
            cookers = ""
            eaters = ""
            for member in group.member_set.all():
                new_choice = int(form.cleaned_data['m_id:' + str(member.id)])

                b_points += 1 if member.last_choice else 0
                a_points += 1 if new_choice else 0
                b_cookers += 1 if member.last_choice > 1 else 0
                a_cookers += 1 if new_choice > 1 else 0

                if new_choice == 1:	
                    eaters = eaters + member.get_name() + " "
                elif new_choice == 2:
                    cookers = cookers + member.get_name() + " "

            if b_cookers > 0:
                b_points_per_cooker = (b_points - b_cookers)/float(b_cookers)
            else:
                b_points_per_cooker = 0

            if a_cookers > 0:
                a_points_per_cooker = (a_points - a_cookers)/float(a_cookers)
            else:
                a_points_per_cooker = 0

            for member in group.member_set.all():
                # update points
                new_choice = int(form.cleaned_data['m_id:' + str(member.id)])
                points = member.points

                # remove point allocations
                if (member.last_choice == 2 and b_points_per_cooker):
                    points -= b_points_per_cooker
                elif (member.last_choice == 1 and b_points_per_cooker):
                    points = points + 1

                # add new points
                if (new_choice == 2):
                    points = points + a_points_per_cooker
                elif (new_choice == 1 and a_points_per_cooker):
                    points = points - 1;

                member.points = points
                member.last_choice = new_choice
                log_save(member, 9)
                member.save()

            messages.add_message(request, messages.INFO, 
                                 'The result has been updated')

            heading = "History update"
            paragraphs = [ 
                "The result of the last group cook has been updated by {}.".format(edit_member.get_name()),
                "The result has been change to"
                ]
            choices={ 2:'chef', 1:'diner', 0:'-' }
            for m in group.member_set.all().order_by('-last_choice', 'user__first_name'):
                paragraphs.append(m.user.first_name + ': ' + choices[m.last_choice])
            content = { 'heading':heading, 'paragraphs':paragraphs }
            body = render_to_string('general_info.html', content)
            subject = 'Update to your groups history'
            from_email = 'house-bot'
            to = [member.user.email for member in 
                  Member.objects.filter(cook_group=group).filter(email_history=True)]
            message = mail.EmailMessage(subject, body, from_email, to)
            message.content_subtype = "html"
            message.send()

            return redirect('/')

    else:
        form = EditHistoryForm(group=group)

    return render(request, 'edit_history.html', { 'form':form })

@login_required
def karma_view(request):
    member = get_object_or_404(Member, user=request.user)   
    return render(request, 'points.html', { 'member':member })

from contact_form.views import ContactFormView
class MyContactFormView(ContactFormView):
    form_class = MyContactForm

    def form_valid(self, form):
        form.save()
        return super(ContactFormView, self).form_valid(form)

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, 
                             'Thankyou, we really appreciate your feedback')
        return reverse('home')

import datetime
def log_save(member, id):
    c = member.choice
    if c == None: c = -1
    now = datetime.datetime.utcnow()
    open('/var/log/save.log','a').write('%d: %s, %s, %s\n'%(id, now.__str__(), member.get_name(), c.__str__()))
