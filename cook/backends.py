from django.core.mail.backends.smtp import EmailBackend
from django.core.mail import mail_admins
import smtplib, time, sys

class MyEmailBackend(EmailBackend):
    def send_messages(self, email_messages):
        """
        Sends one or more EmailMessage objects and returns the number of email
        messages sent.
        """
        if not email_messages:
            return
        with self._lock:
            num_sent = 0
            for message in email_messages:
                attempts = 0
                second_attempts = 0
                while(attempts < 5):
                    try:
                        self.open()
                        if not self.connection:
                            # We failed silently on open().
                            # Trying to send would be pointless.
                            mail_admins('silent fail opening connection',
                                        'failed to send to emails')
                            return

                        sent = self._send(message)
                        if sent:
                            num_sent += 1
                        else:
                            mail_admins('send fail',
                                        'failed to send to {}'.format(message.recipients()))
                        attempts = 5
                    except smtplib.SMTPServerDisconnected:
                        attempts += 1
                        if attempts > 5:
                            mail_admins('send fail - SMTPServerDisconnected',
                                        'failed to send to {}'.format(message.recipients()))
                    except smtplib.SMTPResponseException:
                        attempts += 1
                        if attempts > 5:
                            mail_admins('send fail - SMTPResponseException',
                                        'failed to send to {}'.format(message.recipients()))
                    except smtplib.SMTPConnectError:
                        attempts += 1
                        time.sleep(1)
                        if attempts > 5:
                            mail_admins('send fail - SMTPConnectError',
                                        'mini fail to send to {}'.format(message.recipients()))
                            if second_attempts > 5:
                                mail_admins('send fail - SMTPConnectError',
                                            'failed to send to {}'.format(message.recipients()))
                            else:
                                second_attempts += 1
                                attempts = 0
                    except:
                        mail_admins('send fail - Uncaught exception',
                                    'uncaught exception {}:'.format(sys.exc_info()[0]))                    
                    
        self.close()
        return num_sent
