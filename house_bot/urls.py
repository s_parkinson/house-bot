from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.views.generic import TemplateView, RedirectView

from cook.views import MyContactFormView
from cook.forms import PasswordResetForm

# import html5_appcache
# html5_appcache.autodiscover()

from sitemap import *
sitemaps = {
    'pages':Sitemap(['sign up', 'sign up to group', 
                     'resend', 'create group',
                     'logout', 'login', 'close account', 'edit history', 'karma', 
                     'settings account', 'settings group', 'password update', 'password_reset', 
                     'password_reset_done', 'contact_form', 'quick choice']),
    'verify':Sitemap_Verify(['verify', 'password_reset_confirm']),
}

urlpatterns = patterns(
    '',

    # home
    url(r'^$', 'cook.views.index', name='home'),
    
    # registration
    url(r'^sign_up/$', 'cook.views.sign_up', name='sign up'),
    url(r'^sign_up_to_group/$', 'cook.views.sign_up_to_group', name='sign up to group'),
    url(r'^verify/(?P<uid>[0-9A-Za-z]+)-(?P<token>.+)/$', 'cook.views.verify', name="verify"),
    url(r'^resend/$', 'cook.views.resend_verification', name="resend"),
    url(r'^group_create/$', 'cook.views.create_group', name='create group'),
    url(r'^logout/$', 'cook.views.logout_view', name='logout'),
    url(r'^login/$', 'cook.views.login_view', name='login'),
    url(r'^close_account/$', 'cook.views.close_account', name='close account'),

    # history and karma
    url(r'^group_history/$', 'cook.views.edit_history_view', name='edit history'),
    url(r'^karma/$', 'cook.views.karma_view', name='karma'),

    # choice from email
    url(r'^quick_choice/(?P<uid>[0-9A-Za-z]+)-(?P<choice>[1-5])-(?P<token>.+)/$', 
        'cook.views.quick_choice', name="quick choice"),
    url(r'^quick_choice/(?P<uid>[0-9A-Za-z]+)/$', 
        'cook.views.quick_choice', name="quick choice"),
    url(r'^quick_choice/$', 
        'cook.views.quick_choice', name="quick choice"),

    # account settings
    url(r'^settings/account/$', 'cook.views.settings_account_view', name='settings account'),
    url(r'^settings/group/$', 'cook.views.settings_group_view', name='settings group'),
    url(r'^settings/$', RedirectView.as_view(url='/settings/account')),
    url(r'^password_update/$', 'cook.views.password_update_view', name='password update'),

    # password reset
    url(r'^password_reset/$', 
        'django.contrib.auth.views.password_reset', 
        {'post_reset_redirect' : '/password_reset/done/',
         'password_reset_form' : PasswordResetForm},
        name="password_reset"),
    url(r'^password_reset/done/$',
        'django.contrib.auth.views.password_reset_done'),
    url(r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 
        'django.contrib.auth.views.password_reset_confirm', 
        {'post_reset_redirect' : '/reset/done/'},
        name="password_reset_confirm"),
    url(r'^reset/done/$', 
        'django.contrib.auth.views.password_reset_complete',
        name="password_reset_done"),
    
    # enable admin documentation
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    
    # enable the admin
    url(r'^admin/', include(admin.site.urls)),
    
    # captcha
    url(r'^captcha/', include('captcha.urls')),

    # contact us
    url(r'^contact$', MyContactFormView.as_view(), name='contact_form'),   

    # sitemap
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps }), 

    # cache manifest
    url(r'^manifest\.appcache$', TemplateView.as_view(template_name="manifest.appcache", content_type="text/cache-manifest"), 
        name='manifest'),
    url(r'^offline$', TemplateView.as_view(template_name="offline.html"), name='offline'),
)
