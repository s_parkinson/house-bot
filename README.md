# House-bot #

House-bot is a tool made by [Mettle Studio Ltd.](http://www.mettle-studio.com) with the aim of making eating together and sharing the cooking easy in shared houses. House-Bot finds out who wants in and who's turn it is to cook. 

We love products that make life easier, more social, and more fun. We also love cooking. We spent our university days, and many years after, living in shared accommodation. We always enjoyed eating together with our house mates and sharing stories about our day. However, ​a​s life became more and more hectic, people actually developed social lives outside of the shared house, and London sucked everyone's spare time away, arranging to eat together in the evening became an impossible task. 

A mornings hacking turned Sam's desktop machine into an email based house cooking organisation tool. This was great! We were all eating together again and our lives were richer for it. Then friends started taking an interest in how it could work for them and we realised that other houses have the same problem that we did. House-bot was born!

### The core components ###

* Web app developed using [Django](http://www.djangoproject.com)
* Secure [MySQL](http://www.mysql.com) database for storing group data
* Selection algorithm run as cron job (this is the brains of house-bot)

### Fork me ###

Feel free to fork this repository. The selection algorithm is fairly general and can be modified to organise any group with some kind of rota system. We've had a few ideas

* Who's turn is it to wash the football kit / book the training ground
* Cake baking (and eating) club
* Cleaning rota's
* Arranging group exercise sessions